# NTNU PROG2053 Part 2

Check the project description for what to do.

The project is taken from the Stanford course, CS142, with the permission of the course leader
Mendel Rosenblum.

## Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm run start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm run start-express`
Runs the express server that serves the app.  
Send **GET** requests to [http://localhost:3001](http://localhost:3001) to request content maually. 
Examples:
 - GET [http://localhost:3001/user/list](http://localhost:3001/user/list)
 - GET [http://localhost:3001/test/info](http://localhost:3001/test/info)


## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
