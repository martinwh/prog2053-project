import React from 'react';
import './UserPhotos.css';
import '../Global.css';
import {withRouter} from 'react-router';
import * as path from 'path';
import {Link} from 'react-router-dom';
import fetchModel from '../../../lib/fetchModelData';


/**
 * Define UserPhotos, a React componment of PROG2053 part #2
 */
class UserPhotos extends React.Component {
	constructor(props) {
		super(props);
		this.state = {userPhotos: []};
	}

	componentDidMount() {
		this.fetchUserPhotos();
	}

	componentDidUpdate(prevProps) {
		if (this.props.match.params.userId !== prevProps.match.params.userId) {
			this.fetchUserPhotos();
		}
	}

	fetchUserPhotos() {
		fetchModel(`http://localhost:3001/photosOfUser/${this.props.match.params.userId}`)
			.then((value) => this.setState({userPhotos: value.data}))
			.catch((error) => console.log(error));
	}

	getAuthor = (user) => {
		if (user) {
			return <Link className="text-link"
									 to={`/photo-share/users/${user._id}`}>{user.first_name} {user.last_name}</Link>;
		}
		return '';
	};

	generateComments = (image) => {
		//console.log(image.comments);
		if (image.comments) {
			return (
				<>
					{image.comments.map((comment) => {
						return (
							<div className="comment" key={comment._id}>
								{this.getAuthor(comment.user)} {comment.comment} <br/>
								<span className="comment-date">Commented {comment.date_time}</span>
							</div>
						);
					})}
				</>
			);
		}
		return <></>;
	};

	render() {
		return (
			<>
				<Link to={`/photo-share/users/${this.props.match.params.userId}`}>
					<button type="button" className="button">
						<span>User Info</span>
					</button>
				</Link>
				<div id="divImageList">
					{this.state.userPhotos.map((image) => (
						// eslint-disable-next-line react/jsx-key
						<div className="divImageItem" key={image._id}>
							<div className="divImage">
								<img src={path.join(__dirname, 'images', image.file_name)} alt={image.file_name} />
							</div>
							<div className="divPhotoInfo">
								{this.generateComments(image)}
								<span>Created {image.date_time}</span>
							</div>
						</div>
					))}
				</div>
			</>
		);
	}
}

export default withRouter(UserPhotos);
