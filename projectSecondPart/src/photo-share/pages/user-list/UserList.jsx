import React from 'react';
import {
	Divider,
	List,
	ListItem,
	ListItemText
} from '@material-ui/core';
import './UserList.css';
import {Link} from 'react-router-dom';
import fetchModel from '../../../lib/fetchModelData';

/**
 * Define UserList, a React componment of PROG2053 part #2
 */
class UserList extends React.Component {
	constructor(props) {
		super(props);
		this.state = {userModel: []};
	}

	componentDidMount() {
		fetchModel('http://localhost:3001/user/list')
			.then((value) => {
				this.setState({userModel: value.data});
			})
			.catch((error) => console.log(error));
	}

	displayUsers = () => {
		const users = [];
		this.state.userModel.forEach((e) => {
			users.push((
				<ListItem key={e._id} button component={Link} to={`/photo-share/users/${e._id}`}>
					<ListItemText primary={`${e.first_name} ${e.last_name}`}/>
				</ListItem>
			));
			users.push(<Divider key={`Divider_${e._id}`}/>);
		});
		return users;
	};

	render() {
		return (
			<div>
				<List component="nav">
					{this.displayUsers()}
				</List>
			</div>
		);
	}
}

export default UserList;
