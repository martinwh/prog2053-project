import React from 'react';
import {
	Typography
} from '@material-ui/core';
import {Link, withRouter} from 'react-router-dom';
import './UserDetail.css';
import '../Global.css';
import fetchModel from '../../../lib/fetchModelData';

/**
 * Define UserDetail, a React componment of PROG2053 part #2
 */
class UserDetail extends React.Component {
	constructor(props) {
		super(props);
		this.state = {userDetails: ''};
	}

	componentDidMount() {
		this.fetchUserDetails();
	}

	componentDidUpdate(prevProps) {
		if (this.props.match.params.userId !== prevProps.match.params.userId) {
			this.fetchUserDetails();
		}
	}

	fetchUserDetails() {
		fetchModel(`http://localhost:3001/user/${this.props.match.params.userId}`)
			.then((value) => this.setState({userDetails: value.data}))
			.catch((error) => console.log(error));
	}

	generateUserPreview = () => {
		const user = this.state.userDetails;
		return (
			<>
				<Typography variant="h3">
					{user.first_name} {user.last_name}
				</Typography>
				<Typography variant="body1">
					<span className="profile-description">Occupation:</span> {user.occupation} <br/>
					<span className="profile-description">From:</span> {user.location} <br/>
					<span className="profile-description">Description:</span> {user.description}
				</Typography>
			</>
		);
	};

	render() {
		return (
			<>
				<Link to={`/photo-share/photos/${this.props.match.params.userId}`}>
					<button type="button" className="button">
						<span>Photos</span>
					</button>
				</Link>
				{this.generateUserPreview()}
			</>
		);
	}
}

export default withRouter(UserDetail);
