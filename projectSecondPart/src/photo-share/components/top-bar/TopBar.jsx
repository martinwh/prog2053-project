import React from 'react';
import {
	AppBar, Box, Toolbar, Typography
} from '@material-ui/core';
import './TopBar.css';
import {withRouter} from 'react-router';
import fetchModel from '../../../lib/fetchModelData';

/**
 * Define TopBar, a React componment of PROG2053 part #2
 */
class TopBar extends React.Component {
	constructor(props) {
		super(props);
		this.state = {headerContext: '', name: 'Group 9', version: ''};
	}

	componentDidMount() {
		this.updateHeader(this.props.location.pathname);
		this.fetchInfo();
	}

	componentDidUpdate(prevProps) {
		if (this.props.location.pathname !== prevProps.location.pathname) {
			this.updateHeader(this.props.location.pathname);
		}
	}

	updateHeader = (pathname) => {
		const path = pathname.split('/');
		if (path[2] && path[3]) {
			fetchModel(`http://localhost:3001/user/${path[3]}`)
				.then((value) => {
					const user = value.data;

					// Set context
					let newContext = '';
					if (path[2] === 'photos') {
						newContext += 'Photos of ';
					}
					newContext += `${user.first_name} ${user.last_name}`;
					this.setState({headerContext: newContext});
				})
				.catch((error) => console.log(error));
		}
	};

	fetchInfo() {
		fetchModel('http://localhost:3001/test/info')
			.then((value) => this.setState({version: `v.${value.data.__v}`}))
			.catch((error) => console.log(error));
	}

	render() {
		return (
			<Box sx={{flexGrow: 1}}>
				<AppBar position="fixed">
					<Toolbar className="prog2053-topbar-toolbar">
						<Typography variant="h5" color="inherit" component="div" sx={{flexGrow: 1}} id="left">
							{this.state.name}
						</Typography>
						<Typography variant="h5" color="inherit" component="div" sx={{flexGrow: 1}} id="right">
							{this.state.headerContext}
						</Typography>
						<Typography variant="h5" color="inherit" component="div" sx={{flexGrow: 1}} id="version">
							{this.state.version}
						</Typography>
					</Toolbar>
				</AppBar>
			</Box>
		);
	}
}

export default withRouter(TopBar);
