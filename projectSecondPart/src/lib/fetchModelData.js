/**
	* FetchModel - Fetch a model from the web server.
	*     url - string - The URL to issue the GET request.
	* Returns: a Promise that should be filled
	* with the response of the GET request parsed
	* as a JSON object and returned in the property
	* named "data" of an object.
	* If the requests has an error the promise should be
	* rejected with an object contain the properties:
	*    status:  The HTTP response status
	*    statusText:  The statusText from the xhr request
	*
*/


function fetchModel(url) {
	return new Promise(function (resolve, reject) {
		//console.log(url);
		//setTimeout(() => reject({status: 501, statusText: 'Not Implemented'}), 0);
		const request = new XMLHttpRequest();
		request.open('GET', url, true);
		request.onload = function () {
			if (request.status !== 200) {
				// eslint-disable-next-line prefer-promise-reject-errors
				reject({
					status: request.status,
					statusText: request.statusText
				});
			} else {
				resolve({
					data: JSON.parse(request.response)
				});
			}
		};
		request.send();
	});
}

export default fetchModel;
