:: Source: https://stackoverflow.com/a/27131024

@echo off
if "%~1" neq "_start_" (
  start /B node webServer.js
  cmd /c "%~f0" _start_ %*
  taskkill /IM node.exe /F
  exit /b
)
shift /1
REM rest of script goes here
npm run build:w