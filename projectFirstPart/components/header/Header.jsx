import React from 'react';
import './Header.css';

class Header extends React.Component {
    render() {
        return (
            <div id="header">
                <h1>Personalized header!</h1>
                {/* Content goes here! */}
            </div>
        )
    }
}

export default Header;