import React from 'react';
import './States.css';

/**
 * Define States, a React componment of CS142 project #4 problem #2.  The model
 * data for this view (the state names) is available
 * at window.cs142models.statesModel().
 */
class States extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      states: window.cs142models.statesModel().sort(),
      displayedStates: window.cs142models.statesModel().sort(),
      searchValue: ''
    };
    //console.log('window.cs142models.statesModel()', this.state.states);
  }

  onInputChange = (event) => {
    this.setState({
      searchValue: event.target.value,
      displayedStates: this.state.states.filter((usStates) => usStates.toLowerCase().includes(event.target.value.toLowerCase()))
    });
  }

  render() {
    return (
      <div>
        <input value={this.state.searchValue} onChange={this.onInputChange} />
        <div>{this.state.searchValue}</div>
        {this.state.displayedStates.length === 0 ?
          <div>No matches</div> :
          <ul>
            {this.state.displayedStates.map((usState) =>
              <li key={usState}>{usState}</li>
            )}
          </ul>
        }
      </div>
    );
  }
}

export default States;
