import React from 'react';
import ReactDOM from 'react-dom';
import States from './components/states/States';
import Example from './components/example/Example';
import Header from "./components/header/Header";

class P4 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {activePage: States, nextPage: Example}
    }

    handleButtonClick = () => {
        this.setState({activePage: this.state.nextPage, nextPage: this.state.activePage})
    }

    render() {
        return(
            <div>
                <button type="button" onClick={this.handleButtonClick}>Switch to {this.state.nextPage.name}</button>
                <this.state.activePage />
            </div>
        )
    }
}

ReactDOM.render(
    <div>
        <Header />
        <P4 />
    </div>,
    document.getElementById('reactapp'),
);
