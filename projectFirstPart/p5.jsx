import React from 'react';
import ReactDOM from 'react-dom';
import {HashRouter, Route, Link, Redirect} from "react-router-dom";
import States from './components/states/States';
import Example from './components/example/Example';
import Header from "./components/header/Header";

class P5 extends React.Component {
  render() {
    return (
      <div>
        <HashRouter>
          <Link to="/states">States</Link>
          <Link to="/example">Example</Link>
          <Route exact path="/">
            <Redirect to="/example"/>
          </Route>
          <Route exact path="/example" component={Example}/>
          <Route exact path="/states" component={States}/>
        </HashRouter>
      </div>
    )
  }
}

ReactDOM.render(
  <div>
    <Header />
    <P5 />
  </div>,
  document.getElementById('reactapp'),
);
