# Prog2053 - Project

## How to setup project
1. Clone the project
2. Open into the project directory that you want to work on, e.g.: projectFirstPart, projectSecondPart etc...
3. Open a terminal window at that directory: 
   - Install modules: `npn install`
   - Build the website: `npm run build:w`
     - This command essentially builds the website every thime you change something (:w = watch)
     - If you modify the _webpack.config.js_ file, you need to cancel and run this command again to generate the bundle.
   - Run the webserver: `node webServer.js`
   - How to build and run the website: 
     - On linux/mac you could run this one-liner to build and run the website: `node webServer.js && npn run build:w`
     - On Windows, the simplest way is to open two terminal windows for each command. (You could also run the start.bat file in each of the project directories)
